<?php
namespace Acme\RestBundle\Controller;

use Acme\RestBundle\Entity\Book;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializationContext;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\SecurityExtraBundle\Annotation\Secure;

class BookController extends Controller implements ClassResourceInterface
{
    /**
     * @Rest\View(serializerGroups={"list"})
     */
    public function cgetAction()
    {
        return $this->getDoctrine()->getManager()->getRepository('AcmeRestBundle:Book')->findAll();
    }
    
    /**
     * @Rest\View(serializerGroups={"details"})
     */
    public function getAction($id) 
    {
        if (!($book = $this->getDoctrine()->getManager()->getRepository('AcmeRestBundle:Book')->find($id))) {
            throw new NotFoundHttpException('Ce livre n\'existe pas');
        }
        
        return $book;
    }
    
    public function postAction(Request $request) 
    {
        $book = new Book();
        $book
            ->setTitle($request->request->get('title'))
            ->setBody($request->request->get('body'))
        ;
        $em = $this->getDoctrine()->getManager();
        $em->persist($book);
        $em->flush();
                    
        return new Response('Livre "' . $book->getTitle() . '" ajouté avec succès');
    }
    
    public function putAction(Book $book) 
    {
        $book
            ->setTitle($request->request->get('title'))
            ->setBody($request->request->get('body'))
        ;
        $em = $this->getDoctrine()->getManager();
        $em->persist($book);
        $em->flush();
        
        return new Response('Livre ' . $book->getTitle() . 'mis à jour avec succès');
    }
    
    public function deleteAction(Book $book) 
    {
        return new Response('Livre ' . $book . ' supprimé avec succès');
    }
    
    /**
     * @Rest\View()
     * @Secure(roles="ROLE_USER")
     */
    public function securedAction() 
    {
        return array('Bien venue user !');
    }
}
